# Solicitações

## Propostas Não Emitidas

```md
modelo captaris: 105
```

## Multiplicidade

```md
modelo captaris: 185
```

## Reajuste Faixa Etária/IGP-M

```md
modelo captaris: 263
```

## Venda Proposta Declinada

```md
modelo captaris: 273
```

## Alteração dos Dados Bancários

```md
modelo captaris: 282
```

## Alteração dos Dados Cadastrais

```md
modelo captaris: 291
```

## 2ª via dos Certificados

```md
modelo captaris: 292
```

## Envio Boleto

```md
modelo captaris: 303
```

## Regularizar/Recomandar Parcelas

```md
modelo captaris: 309
```

## Exclusão de Débito - Conta de Terceiros

```md
modelo captaris: 380
```

## Alteração Data de Cobrança

```md
modelo captaris: 870
```

## Alteração Capital Segurado

```md
modelo captaris: 873
```

## Alteração de Beneficiários

```md
modelo captaris: 909
```

## Alteração da Forma de Pagamento

```md
modelo captaris: 911
```

## Alteração de Cobertura - Inclusão/Exclusão de Cônjuge

```md
modelo captaris: 923
```

## Alteração Periodicidade de Pagamento

```md
modelo captaris: 926
```

## Reabilitação

```md
modelo captaris: 2025
```

## Restituição

```md
modelo captaris: 2175
```

## Cancelamento

```md
modelo captaris: 2183
```

## Remissão

```md
modelo captaris: 2753
```

## Sorteio Contemplação Seguro

```md
modelo captaris: 2761
```

## 2ª via de Condições Gerais

```md
modelo captaris: 3026
```

## Simulaão de Valores

```md
modelo captaris: 3051
```

## 2ª via da Proposta de Adesão

```md
modelo captaris: 3060
```

## Aceitação do Seguro

```md
modelo captaris: 3095
```

# Scripts

## Cadastro metadata worflow

```sql
!!!include(scripts/cadastro_metadata_wf.sql)!!!
```

## Consulta captaris

```sql
!!!include(scripts/consulta_captaris.sql)!!!
```

## Consulta de certificados do tipo bilhete

```sql
!!!include(scripts/consulta_certificado_bilhete.sql)!!!
```

## Consulta workflow

```sql
!!!include(scripts/consulta_de_workflow.sql)!!!
```

## Consulta ocorrencia com parcelas pendentes

```sql
!!!include(scripts/consulta_ocorrencia_parcelas_pendentes.sql)!!!
```

## Consulta ocorrência por pessoa

```sql
!!!include(scripts/consulta_ocorrencia_pessoa.sql)!!!
```

## Consulta processos solicitações

```sql
!!!include(scripts/consulta_processos_solicitacoes.sql)!!!
```

## Nova solicitação

```sql
!!!include(scripts/nova-solicitacao.sql)!!!
```

## Nova consulta

```sql
!!!include(scripts/cadastro_consulta_metadata.sql)!!!
```

## Consulta de metadados

```sql
!!!include(scripts/consulta-fluxo-metadado.sql)!!!
```

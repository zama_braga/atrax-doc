BEGIN
  BEGIN TRY
    BEGIN TRANSACTION;
    DECLARE @CO_SEQ_FLUXO_PROCESSO AS INT
    DECLARE @DS_FLUXO_PROCESS0 AS VARCHAR(100)
    DECLARE @DS_FLUXO_SOLICITACAO AS VARCHAR(500)
    DECLARE @CO_WF_MODEL AS INT

    SET @DS_FLUXO_PROCESS0 = 'CANCELAMENTO'
    SET @DS_FLUXO_SOLICITACAO = ''
    SET @CO_WF_MODEL = 0


    SELECT @CO_SEQ_FLUXO_PROCESSO = CO_SEQ_FLUXO_PROCESSO
  FROM [ocorrencia].[TB_FLUXO_PROCESSO]
  WHERE DS_FLUXO_PROCESSO = @DS_FLUXO_PROCESS0


    INSERT [ocorrencia].[TB_FLUXO_SOLICITACAO]
    (CO_FLUXO_PROCESSO,DS_FLUXO_SOLICITACAO, NU_SLA, TX_RESPOSTA_PADRAO, DT_CRIACAO, CO_WF_MODEL)
  VALUES
    (@CO_SEQ_FLUXO_PROCESSO, @DS_FLUXO_SOLICITACAO, 0, 'BLA', GETDATE(), @CO_WF_MODEL)
    COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    DECLARE
      @ErrorMessage NVARCHAR(4000),
      @ErrorNumber INT,
       @ErrorSeverity INT,
       @ErrorState INT,
       @ErrorLine INT,
       @ErrorProcedure NVARCHAR(200);
 
     SELECT
    @ErrorNumber = ERROR_NUMBER(),
    @ErrorSeverity = ERROR_SEVERITY(),
    @ErrorState = ERROR_STATE(),
    @ErrorLine = ERROR_LINE(),
    @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');
     SELECT @ErrorMessage =
       N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
 '				Message: '+ ERROR_MESSAGE();
     RAISERROR
       (
       @ErrorMessage,
       @ErrorSeverity,
       1,
       @ErrorNumber,
       @ErrorSeverity,
       @ErrorState,
       @ErrorProcedure,
       @ErrorLine
       );
     ROLLBACK TRANSACTION;
   END CATCH
END
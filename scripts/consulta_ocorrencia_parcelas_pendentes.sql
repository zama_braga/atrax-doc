SELECT o.*
  FROM [pagamento].[VW_PARCELAS_SEGURO] as ps
        inner join [apolice].[VW_SEGUROS] as s
            on ps.CO_SEQ_CERTIFICADO = s.CO_CERTIFICADO
        inner join [ocorrencia].[VW_OCORRENCIAS] o
            on s.NU_CPF_CNPJ = o.NU_CPF_CNPJ
  WHERE ps.CO_SITUACAO = 2 and o.CO_SITUACAO = 1


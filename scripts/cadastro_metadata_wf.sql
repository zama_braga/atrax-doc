
BEGIN
  BEGIN TRY
    BEGIN TRANSACTION;
    DECLARE @DS_NAME AS VARCHAR(50)
    DECLARE @DS_DESCRIPTION AS VARCHAR(200)
    DECLARE @CO_PROCESS_SOLICITATION AS INT


    SET @DS_NAME = 'SeguroEmitido'
    SET @DS_DESCRIPTION = 'Seguro encontra-se nas situações AGUARDANDO CRIVO, AGUARDA PGTO 1A PARCELA, EM CRITICA, AGUARDA PROPOSTA FISICA, ACEITO. AGUARDA EMISSAO, DECLINADO POR DECURSO DE PRAZO, NAO ACEITO, A DEBITAR, DEBITO JA ENVIADO'
    SET @CO_PROCESS_SOLICITATION = 2

    INSERT workflow.TB_WF_METADATA(DS_NAME, DS_DESCRIPTION, DT_CREATION) 
        VALUES (@DS_NAME, @DS_DESCRIPTION, GETDATE())


    INSERT workflow.TB_WF_PROCESS_SOLICITATION_METADATA (CO_PROCESS_SOLICITATION, CO_WF_METADATA, DT_CREATION)
            VALUES (@CO_PROCESS_SOLICITATION, (SELECT CO_SEQ_WF_METADATA FROM workflow.TB_WF_METADATA WHERE DS_NAME = @DS_NAME), GETDATE())
    

    SELECT * FROM workflow.TB_WF_METADATA ORDER BY CO_SEQ_WF_METADATA DESC

    COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    DECLARE
      @ErrorMessage NVARCHAR(4000),
      @ErrorNumber INT,
       @ErrorSeverity INT,
       @ErrorState INT,
       @ErrorLine INT,
       @ErrorProcedure NVARCHAR(200);
 
     SELECT
       @ErrorNumber = ERROR_NUMBER(),
       @ErrorSeverity = ERROR_SEVERITY(),
       @ErrorState = ERROR_STATE(),
       @ErrorLine = ERROR_LINE(),
       @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');
     SELECT @ErrorMessage =
       N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
 '				Message: '+ ERROR_MESSAGE();
     RAISERROR
       (
       @ErrorMessage,
       @ErrorSeverity,
       1,
       @ErrorNumber,
       @ErrorSeverity,
       @ErrorState,
       @ErrorProcedure,
       @ErrorLine
       );
     ROLLBACK TRANSACTION;
   END CATCH
 END



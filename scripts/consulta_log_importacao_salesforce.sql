DECLARE @tag as varchar(50)

SELECT @tag = '#salesforce-'+ FORMAT(GETDATE(), 'dd/MM/yyyy', 'en-US' )   +'%'

SELECT 
    min([Date]) as inicio,
    max([Date]) as fim, 
    DATEDIFF(minute, min([Date]), max([Date])) as tempoMinutos,
    DATEDIFF(hour, min([Date]), max([Date])) as tempoHoras,
    (select count(*) FROM [DB_ATRAX].[dbo].[TB_LogAtrax] WHERE MESSAGE LIKE @tag and EXCEPTION = '' ) sucessos,
    (select count(*) FROM [DB_ATRAX].[dbo].[TB_LogAtrax] WHERE MESSAGE LIKE @tag and EXCEPTION <> '' ) erros,
    count(*) as total
FROM [DB_ATRAX].[dbo].[TB_LogAtrax]
WHERE MESSAGE LIKE @tag

SELECT [CO_SEQ_LOG_ATRAX]    
    ,[Date]
    ,[MESSAGE]
    ,[EXCEPTION]
FROM [DB_ATRAX].[dbo].[TB_LogAtrax]
WHERE MESSAGE LIKE @tag
order by [DATE] desc

--delete from [DB_ATRAX].[dbo].[TB_LogAtrax]


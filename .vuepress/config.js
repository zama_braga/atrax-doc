console.log(process.cwd());
module.exports = {
  title: 'Documentação',
  desciption: '',
  dest: './docs',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Definições', link: '/definicoes/' },
      { text: 'Scripts', link: '/scripts/' },
      { text: 'Solicitações', link: '/solicitacoes/' },
      { text: 'Comandos SIAS', link: '/sias/' }
    ],
    sidebar: {
      '/sias/': [
        {
          title: 'Comandos SIAS',
          collapsable: false,
          children: ['certificado', 'bilhete', 'gerais']
        }
      ],
      '/solicitacoes/': [
        {
          title: '',
          collapsable: false,
          children: ['']
        }
      ],
      '/definicoes/': [
        {
          title: '',
          collapsable: false,
          children: ['']
        }
      ],
      '/scripts/': [
        {
          title: '',
          collapsable: false,
          children: ['']
        }
      ]
    },
    lastUpdated: 'Última atualização'
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@images': './assets/images'
      }
    }
  },
  markdown: {
    config: md => {
      // use more markdown-it plugins!
      md.use(require('markdown-it-include'));
    }
  }
};

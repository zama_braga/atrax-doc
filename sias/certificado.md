# Certificados

## Dados Gerais

```md
17 06 05
```

## Alterar Dados Gerais

```md
02 01 02
```

## Alterar Forma de Pagamento

```md
04 19 02
```

## Alterar Beneficiarios

```md
04 06 01
```

## Inclusão/Alteração/Exclusão Parcelas

```md
17 04 15 06
```

## Alteração/Incluir Capital Seguro

```md
04 19 06 01
```

## Alteração/Incluir Capital Seguro Empresarial

```md
04 19 10 02
```

## Alterar dados seguro em situação critica

```md
04 19 01 01
```

## Alterar modo de envio

```md
17 04 15 06
```

## Alterar periodicidade

```md
04 05 17
```

## Cancelar dados seguro em situação critica

```md
04 19 01 03
```

## Inclusão exclusão de conjuge

```md
04 19 10 02
```
